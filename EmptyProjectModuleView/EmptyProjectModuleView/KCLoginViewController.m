//
//  KCLoginViewController.m
//  EmptyProjectModuleView
//
//  Created by YannisChou on 16/3/20.
//  Copyright © 2016年 Alibaba. All rights reserved.
//

#import "KCLoginViewController.h"
#import "KCMainViewController.h"

@interface KCLoginViewController (){
    UITextField *_txtUserName;
    UITextField *_txtPassword;
}

@end

@implementation KCLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addLoginForm];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addLoginForm{
    // 用户名
    UILabel *lbUserName = [[UILabel alloc] initWithFrame:CGRectMake(50, 150, 100, 30)];
    lbUserName.text = @"用户名:";
    [self.view addSubview:lbUserName];
    
    _txtUserName = [[UITextField alloc] initWithFrame:CGRectMake(120, 150, 150, 30)];
    _txtUserName.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:_txtUserName];
    
    // 密码
    UILabel *lbPassword = [[UILabel alloc] initWithFrame:CGRectMake(50, 200, 100, 30)];
    lbPassword.text = @"密码:";
    [self.view addSubview:lbPassword];
    
    _txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(120, 200, 150, 30)];
    _txtPassword.secureTextEntry = YES;
    _txtPassword.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:_txtPassword];
    
    // 登陆按钮
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeSystem];
    btnLogin.frame = CGRectMake(120, 270, 80, 30);
    [btnLogin setTitle:@"登陆" forState:UIControlStateNormal];
    [self.view addSubview:btnLogin];
    [btnLogin addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    
    // 取消登陆按钮
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    btnCancel.frame = CGRectMake(170, 270, 80, 30);
    [btnCancel setTitle:@"取消" forState:UIControlStateNormal];
    [self.view addSubview:btnCancel];
    [btnCancel addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark 登陆操作
-(void)login{
    if ([_txtUserName.text isEqualToString:@"LewisJoe"] && [_txtPassword.text isEqualToString:@"123"]) {
        // 调用代理方法传递参数
        [self.delegate showUserInfoWithUserName:_txtUserName.text];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        // 登陆失败弹出提示信息
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"系统信息" message:@"用户名或密码错误，请重新输入!" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark 点击取消
-(void)cancel{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
