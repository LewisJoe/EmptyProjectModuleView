//
//  KCMeViewController.h
//  EmptyProjectModuleView
//
//  Created by YannisChou on 16/3/20.
//  Copyright © 2016年 Alibaba. All rights reserved.
//
//  展示用户信息的视图控制器
//

#import <UIKit/UIKit.h>

@interface KCMeViewController : UIViewController

#pragma mark 需要传递的属性参数(很多时候它时一个数据模型)
@property (nonatomic,copy) NSString *userInfo;

@end
