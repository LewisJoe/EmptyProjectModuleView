//
//  KCMainViewController.m
//  EmptyProjectModuleView
//
//  Created by YannisChou on 16/3/20.
//  Copyright © 2016年 Alibaba. All rights reserved.
//

#import "KCMainViewController.h"
#import "KCLoginViewController.h"
#import "KCMeViewController.h"

@interface KCMainViewController ()<KCMainDelete,UIActionSheetDelegate>{
    UILabel *_loginInfo;
    UIBarButtonItem *_loginButton;
    UIBarButtonItem *_meButton;
    BOOL _isLogin;
}

@end

@implementation KCMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addNavigationBar];
    
    [self addLoginInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 添加信息显示
-(void)addLoginInfo{
    _loginInfo = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 320, 30)];
    _loginInfo.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_loginInfo];
}

#pragma mark 添加导航栏
-(void)addNavigationBar{
    // 创建一个导航栏
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    // navigationBar.tintColor = [UIColor whiteColor];
    [self.view addSubview:navigationBar];
    // 创建导航控件内容
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"Web Chat"];
    
    // 左侧添加登陆按钮
    _loginButton = [[UIBarButtonItem alloc] initWithTitle:@"登陆" style:UIBarButtonItemStyleDone target:self action:@selector(login)];
    navigationItem.leftBarButtonItem = _loginButton;
    
    // 添加右侧导航按钮
    _meButton = [[UIBarButtonItem alloc] initWithTitle:@"我" style:UIBarButtonItemStyleDone target:self action:@selector(showInfo)];
    _meButton.enabled = NO;
    navigationItem.rightBarButtonItem = _meButton;
    
    // 添加内容到导航栏
    [navigationBar pushNavigationItem:navigationItem animated:NO];
}

#pragma mark 登陆操作
-(void)login{
    if (!_isLogin) {
        KCLoginViewController *loginController = [[KCLoginViewController alloc] init];
        loginController.delegate = self;
        // 调用此方法显示模态窗口
        [self presentViewController:loginController animated:YES completion:nil];
    }else{
        // 如果登陆之后则处理注销的情况
        // 注意当前视图控制器必须实现UIActionSheet代理才能进行操作
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"系统信息" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"注销" otherButtonTitles:nil];
        [actionSheet showInView:self.view];
    }
}

#pragma mark 点击查看我的信息
-(void)showInfo{
    if (_isLogin) {
        KCMeViewController *meController = [[KCMeViewController alloc] init];
        meController.userInfo = _loginInfo.text;
        [self presentViewController:meController animated:YES completion:nil];
    }
}


#pragma mark 实现代理方法
-(void)showUserInfoWithUserName:(NSString *)userName{
    _isLogin = YES;
    // 显示登陆用户信息
    _loginInfo.text = [NSString stringWithFormat:@"Hello,%@!",userName];
    // 登陆按钮内容更改为“注销”
    _loginButton.title = @"注销";
    _meButton.enabled = YES;
}

#pragma mark 实现注销方法
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {// 注销按钮
        _isLogin = NO;
        _loginButton.title = @"登陆";
        _loginInfo.text = @"";
        _meButton.enabled = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
