//
//  AppDelegate.h
//  EmptyProjectModuleView
//
//  Created by YannisChou on 16/3/20.
//  Copyright © 2016年 Alibaba. All rights reserved.
//
//  模态窗口只是视图控制器显示的一种方式(在IOS中并没有专门的模态窗口类)，模态窗口不依赖于控制器容器(例如UITabBarController和
//  UINavigationController)，通常用于显示独立的内容，在模态窗口显示的时候其他视图的内容无法进行操作
//
//  模态窗口使用起来比较容易，一般的视图控制器只要调用-(void)presentViewController:(UIViewController *)viewControllerToPresent
//  animated:(BOOL)flag completion:(void(^)(void)completion NS_AVAILABLE(5_0));方法那么参数中的视图控制器就会以模态窗口的形式展现
//  同时调用-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void(^)(void))completion NS_AVAILABLE_IOS(5_0);方法
//  就会关闭模态窗口
//
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

