//
//  KCLoginViewController.h
//  EmptyProjectModuleView
//
//  Created by YannisChou on 16/3/20.
//  Copyright © 2016年 Alibaba. All rights reserved.
//
//  登陆界面，在界面中只有两个输入框和一个登陆按钮
//

#import <UIKit/UIKit.h>
@protocol KCMainDelete;

@interface KCLoginViewController : UIViewController

#pragma mark 定义代理
@property (nonatomic,strong) id<KCMainDelete> delegate;

@end
