//
//  KCMainViewController.h
//  EmptyProjectModuleView
//
//  Created by YannisChou on 16/3/20.
//  Copyright © 2016年 Alibaba. All rights reserved.
//
//  主界面视图控制器,在左上角放一个登陆按钮用于弹出登录界面
//
//  在IOS开发中常用的参数传递有一下几种方法
//  1.采用代理模式
//  2.采用IOS消息机制
//  3.通过NSDefault存储(或者文件、数据库存储等)
//  4.通过AppDelegate定义全局变量(或者使用UIApplication、定义一个单例类等)
//  5.通过控制器属性传递
//

#import <UIKit/UIKit.h>

#pragma mark 定义一个协议用于参数传递
@protocol KCMainDelete
-(void)showUserInfoWithUserName:(NSString *)userName;
@end

@interface KCMainViewController : UIViewController

@end
